//
//                       _oo0oo_
//                      o8888888o
//                      88" . "88
//                      (| -_- |)
//                      0\  =  /0
//                    ___/`---'\___
//                  .' \\|     |// '.
//                 / \\|||  :  |||// \
//                / _||||| -:- |||||- \
//               |   | \\\  -  /// |   |
//               | \_|  ''\---/''  |_/ |
//               \  .-\__  '-'  ___/-. /
//             ___'. .'  /--.--\  `. .'___
//          ."" '<  `.___\_<|>_/___.' >' "".
//         | | :  `- \`.;`\ _ /`;.`/ - ` : | |
//         \  \ `_.   \_ __\ /__ _/   .-` /  /
//     =====`-.____`.___ \_____/___.-`___.-'=====
//                       `=---='
//
//
//     ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//
//               佛祖保佑         永無BUG
//
//
//
#include<bits/stdc++.h>
#define MAX_N 1000

using namespace std;
typedef long long ll;

const ll MOD = 998244353;
int n, m, r, c;
inline ll add(ll a, ll b){
	return (a + b) % MOD;
}
inline ll sub(ll a, ll b){
	ll c = (a - b) % MOD;
	if(c < 0) c += MOD;
	return c;
}
inline ll mul(ll a, ll b){
	return a * b % MOD;
}
struct P{
	int v;
	ll x, y;
	P(){}
	P(int v, ll x, ll y) : v(v), x(x), y(y) {}
};
vector<P> s;

bool cmp(const P& a, const P& b){
	return a.v > b.v;
}
ll extgcd(ll a, ll b, ll& x, ll& y){
	ll d = a;
	if(b != 0){
		d = extgcd(b, a % b, y, x);
		y -= (a / b) * x;
	}else{
		x = 1, y = 0;
	}
	return d;
}
ll inv(ll q){
	ll x, y;
	extgcd(q, MOD, x, y);
	return (MOD + x % MOD) % MOD;
}
ll calc(ll Pr, ll Prx, ll Pry, ll Prxy, ll Pr_E, P p){
	ll res = Pr_E;
	res = add(res, mul(p.x * p.x + p.y * p.y, Pr));
	res = sub(res, mul(2 * p.x, Prx));
	res = sub(res, mul(2 * p.y, Pry));
	res = add(res, Prxy);
	return res;
}
void solve(){
	sort(s.begin(), s.end(), cmp);

	int i = 0;
	while(s[i].x != r || s[i].y != c) i++;
	while(++i < s.size() && s[i].v == s[i - 1].v);

	ll Pr   = inv(s.size() - i),
       Prx  = mul(Pr, r),
       Pry  = mul(Pr, c),
       Prxy = mul(Pr, r * r + c * c),
       Pr_E = 0,
       E = 0;
	for(int j = i; i < s.size();){
		// printf("%I64d %I64d %I64d %I64d %I64d\n", Pr, Prx, Pry, Prxy, Pr_E);
		E = 0;
		do{
			E = add(E, calc(Pr, Prx, Pry, Prxy, Pr_E, s[i]));//current node's Expected value
		}while(++i < s.size() && s[i].v == s[i - 1].v);

		// if(i == s.size()) break;

		ll _p = inv(s.size() - i);
		ll Prob_to_next = mul(Pr, _p);
		for(; j < i; j++){
			Pr   = add(Pr, Prob_to_next);
			Prx  = add(Prx, mul(Prob_to_next, s[j].x));
			Pry  = add(Pry, mul(Prob_to_next, s[j].y));
			Prxy = add(Prxy, mul(Prob_to_next, s[j].x * s[j].x + s[j].y * s[j].y));
		}
		Pr_E = add(Pr_E, mul(_p, E)); 
	}
	cout << E << endl;
}
int main(){
	FILE* in = fopen("input.txt", "r");
	scanf("%d %d", &n, &m);
	for(int i = 0; i < n; i++){
		for(int j = 0, tmp; j < m; j++){
			scanf("%d", &tmp);
			s.push_back(P(tmp, i, j));
		}
	}
	scanf("%d %d", &r, &c);
	r--; c--;
	solve();

	return 0;
}