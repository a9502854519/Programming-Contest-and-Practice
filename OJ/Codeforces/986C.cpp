//
//                       _oo0oo_
//                      o8888888o
//                      88" . "88
//                      (| -_- |)
//                      0\  =  /0
//                    ___/`---'\___
//                  .' \\|     |// '.
//                 / \\|||  :  |||// \
//                / _||||| -:- |||||- \
//               |   | \\\  -  /// |   |
//               | \_|  ''\---/''  |_/ |
//               \  .-\__  '-'  ___/-. /
//             ___'. .'  /--.--\  `. .'___
//          ."" '<  `.___\_<|>_/___.' >' "".
//         | | :  `- \`.;`\ _ /`;.`/ - ` : | |
//         \  \ `_.   \_ __\ /__ _/   .-` /  /
//     =====`-.____`.___ \_____/___.-`___.-'=====
//                       `=---='
//
//
//     ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//
//               佛祖保佑         永無BUG
//
//
//
#include<bits/stdc++.h>
#define MAX_N 22

using namespace std;

int n, m;
unsigned int a[1 << MAX_N], U;
bool used[1 << MAX_N], exist[1 << MAX_N];

void dfs(unsigned int v){
	if(used[v]) return;
	used[v] = true;

	if(exist[v]){
		dfs(U ^ v);
	}
	for(int i = 0; i < n; i++){
		if(v & (1 << i)) dfs(v ^ (1 << i));
	}
}
void solve(){
	int res = 0;
	for(int i = 0; i < m; i++){
		if(!used[a[i]]){
			used[a[i]] = true;
			dfs(U ^ a[i]);
			res++;
		}
	}
	cout << res << endl;
}
int main(){
	cin >> n >> m;
	U = (1 << n) - 1;
	for(int i = 0; i < m; i++){
		scanf("%d", a + i);
		exist[a[i]] = true;
	}
	solve();
	return 0;
}