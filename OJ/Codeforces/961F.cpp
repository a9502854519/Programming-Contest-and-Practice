//
//                       _oo0oo_
//                      o8888888o
//                      88" . "88
//                      (| -_- |)
//                      0\  =  /0
//                    ___/`---'\___
//                  .' \\|     |// '.
//                 / \\|||  :  |||// \
//                / _||||| -:- |||||- \
//               |   | \\\  -  /// |   |
//               | \_|  ''\---/''  |_/ |
//               \  .-\__  '-'  ___/-. /
//             ___'. .'  /--.--\  `. .'___
//          ."" '<  `.___\_<|>_/___.' >' "".
//         | | :  `- \`.;`\ _ /`;.`/ - ` : | |
//         \  \ `_.   \_ __\ /__ _/   .-` /  /
//     =====`-.____`.___ \_____/___.-`___.-'=====
//                       `=---='
//
//
//     ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//
//               佛祖保佑         永無BUG
//
//
//
#include<bits/stdc++.h>
#define MAX_N 1000010
#define MAX_Spar_SIZE (1 << 20)
#define INF 1E9

using namespace std;
typedef long long ll;

const ll B = 131, MOD = 1e9 + 9;
int n, k;
char s[MAX_N];
int res[MAX_N / 2];
ll h[MAX_N], t[MAX_N];

ll mul(ll a, ll b){
	return a * b % MOD;
}
void construct_hash(){
	h[0] = s[0] - 'a' + 1;
	t[0] = 1;
	for(int i = 1; i < n; i++){
		h[i] = (mul(h[i - 1], B) + s[i] - 'a' + 1) % MOD;
		t[i] = mul(t[i - 1], B);
	}
}
ll norm(ll a){
	a %= MOD;
	if(a <= 0) a += MOD;
	return a;
}
ll get(int len, int i){
	return norm(h[i + len - 1] - (i == 0 ? 0 : mul(h[i - 1], t[len])));
}
bool C(int len, int i){
	int j = n - i - len;
	return get(len, i) == get(len, j);
}
void solve(){
	construct_hash();
	memset(res, -1, sizeof(res));
	for(int i = n / 2 - 1; i >= 0; i--){
		int &len = res[i] = res[i + 1] + 2;
		while(len > 0){
			if(C(len, i)) break;
			len -= 2;
		}
	}
	for(int i = 0; i < (n + 1) / 2; i++){
		// if(i > 0) res[i] = max(res[i], res[i - 1] - 2);
		printf("%d ", res[i]);
	}
}
int main(){
	scanf("%d %s", &n, s);
	solve();
    return 0;
}