//
//                       _oo0oo_
//                      o8888888o
//                      88" . "88
//                      (| -_- |)
//                      0\  =  /0
//                    ___/`---'\___
//                  .' \\|     |// '.
//                 / \\|||  :  |||// \
//                / _||||| -:- |||||- \
//               |   | \\\  -  /// |   |
//               | \_|  ''\---/''  |_/ |
//               \  .-\__  '-'  ___/-. /
//             ___'. .'  /--.--\  `. .'___
//          ."" '<  `.___\_<|>_/___.' >' "".
//         | | :  `- \`.;`\ _ /`;.`/ - ` : | |
//         \  \ `_.   \_ __\ /__ _/   .-` /  /
//     =====`-.____`.___ \_____/___.-`___.-'=====
//                       `=---='
//
//
//     ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//
//               佛祖保佑         永無BUG
//
//
//
#include<bits/stdc++.h>
#define MAX_N 200000 + 10
using namespace std;
typedef long long ll;

const ll B = 131;
const ll P = 1e9 + 9;
int n, m;
char s[MAX_N];
ll  h[26][MAX_N];
ll t[MAX_N];

ll add(ll a, ll b){
	return (a + b) % P;
}
ll sub(ll a, ll b){
	ll c = (a - b) % P;
	if(c < 0) c += P;
	return c;
}
ll mul(ll a, ll b){
	return a * b % P;
}

bool calc(int x, int y, int len){
	ll a[26], b[26];

	for(int i = 0; i < 26; i++){
		a[i] = sub(h[i][x + len - 1], mul(x > 0 ? h[i][x - 1] : 0, t[len]));
		b[i] = sub(h[i][y + len - 1], mul(y > 0 ? h[i][y - 1] : 0, t[len]));
	}
	sort(a, a + 26); sort(b, b + 26);
	for(int i = 0; i < 26; i++){
		if(a[i] != b[i]) return false;
	}
	return true;
}
void solve(){
	t[0] = 1;
	for(int i = 1; i < n; i++){
		t[i] = mul(t[i - 1], B);
	}
	for(int i = 0; i < 26; i++){
		h[i][0] = (s[0] == i + 'a');
		for(int j = 1; j < n; j++){
			h[i][j] = add(mul(h[i][j - 1], B), s[j] == i + 'a');
		}
	}
	for(int x, y, len; m--; ){
		scanf("%d %d %d", &x, &y, &len);
		puts(calc(x - 1, y - 1, len) ? "YES" : "NO");
	}
}
int main(){
	scanf("%d %d %s", &n, &m, s);
	solve();
	return 0;
}