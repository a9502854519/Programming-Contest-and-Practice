//
//                       _oo0oo_
//                      o8888888o
//                      88" . "88
//                      (| -_- |)
//                      0\  =  /0
//                    ___/`---'\___
//                  .' \\|     |// '.
//                 / \\|||  :  |||// \
//                / _||||| -:- |||||- \
//               |   | \\\  -  /// |   |
//               | \_|  ''\---/''  |_/ |
//               \  .-\__  '-'  ___/-. /
//             ___'. .'  /--.--\  `. .'___
//          ."" '<  `.___\_<|>_/___.' >' "".
//         | | :  `- \`.;`\ _ /`;.`/ - ` : | |
//         \  \ `_.   \_ __\ /__ _/   .-` /  /
//     =====`-.____`.___ \_____/___.-`___.-'=====
//                       `=---='
//
//
//     ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//
//               佛祖保佑         永無BUG
//
//
//
#include<bits/stdc++.h>
#define MAX_N 200010

using namespace std;
typedef long long ll;

int n;
ll t, a[MAX_N], res;

vector<ll> dfs(int l, int r){
	vector<ll> tmp;
	if(r - l == 1){
		tmp.push_back(a[l]);
		res += (a[l] < t);
	}else{
		vector<ll> left = dfs(l, (l + r) / 2);
		vector<ll> right = dfs((l + r) / 2, r);

		// printf("[%d, %d)\n", l, r);

		ll sum = 0;
		for(int i = l; i < (l + r) / 2; i++) sum += a[i];
	
		// for(ll i : left) cout << i << " ";
		// cout << endl;
		// for(ll i 。: right) cout << i << " ";
		// cout << "\n";


		// cout << res << endl;
		int pl = left.size() - 1, pr = right.size() - 1;
		while(pl >= 0 && pr >= 0){
			while(pr >= 0 && sum - left[pl] + right[pr] >= t) pr--;
			res += pr + 1;
			pl--;
		}
		// cout << res << endl;

		// pr = right.size() - 1;
		// for(int i = 0, cnt = 0; cnt < 2 && pr >= 0; i += sum, cnt++){
		// 	while(pr >= 0 && i + right[pr] >= t) pr--;
		// 	if(i == 0) res -= pr + 1;
		// 	else res += pr + 1;
		// }
		pr = right.size() - 1;
		while(pr >= 0 && right[pr] >= t) pr--;
		res -= pr + 1;

		pr = right.size() - 1;
		while(pr >= 0 && sum + right[pr] >= t) pr--;
		res += pr + 1;
		
		// cout << res << "\n\n";
		for(ll &i : right) i += sum;
		tmp.resize(left.size() + right.size());
		merge(left.begin(), left.end(), right.begin(), right.end(), tmp.begin());
	}
	return tmp;
}
void solve(){
	dfs(0, n);
	cout << res << endl;
}
int main(){
	cin >> n >> t;
	for(int i = 0; i < n; i++){
		scanf("%I64d", a + i);
	}
	solve();
    return 0;
}